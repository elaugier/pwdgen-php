<?php
/**
 * Created by PhpStorm.
 * User: ela
 * Date: 08/03/2015
 * Time: 20:52
 */

namespace Pwdgen;


class Pwdgen
{

    private $bNumber = true;
    private $bUpper = true;
    private $bLower = true;
    private $bSpecial = true;
    private $bRepetition = true;
    private $bSimilarCharacters = true;
    private $iLength = 16;

    public function generatePassword()
    {
        $pwd = "";
        $authorizedcharacters = "";
        if ($this->bUpper) $authorizedcharacters .= "ABCDEFGHJKLMNPQRSTUVWXYZ";
        if ($this->bLower) $authorizedcharacters .= "abdcefghijkmnopqrstuvwxyz";
        if ($this->bNumber) $authorizedcharacters .= "23456789";
        if ($this->bSpecial) $authorizedcharacters .= "~!@#$%^&*(-_=+[]{};:,.<>/?";
        if ($this->bSimilarCharacters && $this->bNumber) $authorizedcharacters .= "01";
        if ($this->bSimilarCharacters && $this->bUpper) $authorizedcharacters .= "OI";
        if ($this->bSimilarCharacters && $this->bUpper) $authorizedcharacters .= "l";
        if ($authorizedcharacters == "") {
            echo "Too many options deselected. Reset to default";
            $authorizedcharacters = "ABCDEFGHJKLMNPQRSTUVWXYZabdcefghijkmnopqrstuvwxyz23456789~!@#$%^&*(-_=+[]{};:,.<>/?01OIl";
        }

        srand($this->make_seed());
        $c2 = null;
        for ($count = 0; $count < $this->iLength; $count++) {
            $c = null;
            do {
                $c = rand(0, strlen($authorizedcharacters) - 1);
            } while (($c == $c2) && $this->bRepetition);
            $c2 = $c;
            $pwd .= substr($authorizedcharacters, $c, 1);
        }
        return $pwd;
    }

    private function make_seed()
    {
        list($usec, $sec) = explode(' ', microtime());
        return (float)$sec + ((float)$usec * 100000);
    }

    /**
     * @return boolean
     */
    public function isBNumber()
    {
        return $this->bNumber;
    }

    /**
     * @param boolean $bNumber
     */
    public function setBNumber($bNumber)
    {
        $this->bNumber = $bNumber;
    }

    /**
     * @return boolean
     */
    public function isBUpper()
    {
        return $this->bUpper;
    }

    /**
     * @param boolean $bUpper
     */
    public function setBUpper($bUpper)
    {
        $this->bUpper = $bUpper;
    }

    /**
     * @return boolean
     */
    public function isBLower()
    {
        return $this->bLower;
    }

    /**
     * @param boolean $bLower
     */
    public function setBLower($bLower)
    {
        $this->bLower = $bLower;
    }

    /**
     * @return boolean
     */
    public function isBSpecial()
    {
        return $this->bSpecial;
    }

    /**
     * @param boolean $bSpecial
     */
    public function setBSpecial($bSpecial)
    {
        $this->bSpecial = $bSpecial;
    }

    /**
     * @return boolean
     */
    public function isBRepetition()
    {
        return $this->bRepetition;
    }

    /**
     * @param boolean $bRepetition
     */
    public function setBRepetition($bRepetition)
    {
        $this->bRepetition = $bRepetition;
    }

    /**
     * @return boolean
     */
    public function isBSimilarCharacters()
    {
        return $this->bSimilarCharacters;
    }

    /**
     * @param boolean $bSimilarCharacters
     */
    public function setBSimilarCharacters($bSimilarCharacters)
    {
        $this->bSimilarCharacters = $bSimilarCharacters;
    }

    /**
     * @return int
     */
    public function getILength()
    {
        return $this->iLength;
    }

    /**
     * @param int $iLength
     */
    public function setILength($iLength)
    {
        if ($iLength > 999) die('Too big !');
        $this->iLength = $iLength;
    }
}