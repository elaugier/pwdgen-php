<?php
/**
 * Created by PhpStorm.
 * User: ela
 * Date: 08/03/2015
 * Time: 20:35
 */
require_once 'Pwdgen.php';
$pwdgen = new \Pwdgen\Pwdgen();
if (isset($_POST['Number'])) ($_POST['Number'] == "on") ? $pwdgen->setBNumber(true) : $pwdgen->setBNumber(false);
else $pwdgen->setBNumber(false);
if (isset($_POST['UpperCase'])) ($_POST['UpperCase'] == "on") ? $pwdgen->setBUpper(true) : $pwdgen->setBUpper(false);
else $pwdgen->setBUpper(false);
if (isset($_POST['LowerCase'])) ($_POST['LowerCase'] == "on") ? $pwdgen->setBLower(true) : $pwdgen->setBLower(false);
else $pwdgen->setBLower(false);
if (isset($_POST['Special'])) ($_POST['Special'] == "on") ? $pwdgen->setBSpecial(true) : $pwdgen->setBSpecial(false);
else $pwdgen->setBSpecial(false);
if (isset($_POST['Repetition'])) ($_POST['Repetition'] == "on") ? $pwdgen->setBRepetition(true) : $pwdgen->setBRepetition(false);
else $pwdgen->setBRepetition(false);
if (isset($_POST['SimilarCharacters'])) ($_POST['SimilarCharacters'] == "on") ? $pwdgen->setBSimilarCharacters(true) : $pwdgen->setBSimilarCharacters(false);
else $pwdgen->setBSimilarCharacters(false);
if (isset($_POST['Length'])) $pwdgen->setILength($_POST['Length']);
$password = $pwdgen->generatePassword();
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <title></title>
</head>
<body>
<h1 class="text-center"><?= htmlentities("Générateur de mot de passe") ?></h1>
<hr>
<div class="text-center"><strong>
        <pre><?php echo $password; ?></pre>
    </strong></div>
<hr>
<div>
    <form method="post" action="/">
        <ul class="list-group">
            <li class="list-group-item text-center"><input class="btn btn-default" type="submit" name="Generate"
                                                           value="Generate"></li>
            <li class="list-group-item text-center"><label for="Number">Numbers : </label><input name="Number"
                                                                                                 type="checkbox" <?= $pwdgen->isBNumber() ? 'checked' : '' ?>>
            </li>
            <li class="list-group-item text-center"><label for="UpperCase">UpperCase : </label><input name="UpperCase"
                                                                                                      type="checkbox" <?= $pwdgen->isBUpper() ? 'checked' : '' ?>>
            </li>
            <li class="list-group-item text-center"><label for="LowerCase">LowerCase : </label><input name="LowerCase"
                                                                                                      type="checkbox" <?= $pwdgen->isBLower() ? 'checked' : '' ?>>
            </li>
            <li class="list-group-item text-center"><label for="Special">Specials : </label><input name="Special"
                                                                                                   type="checkbox" <?= $pwdgen->isBSpecial() ? 'checked' : '' ?>>
            </li>
            <li class="list-group-item text-center"><label for="Repetition">Repetition : </label><input
                    name="Repetition" type="checkbox" <?= $pwdgen->isBRepetition() ? 'checked' : '' ?>></li>
            <li class="list-group-item text-center"><label for="SimilarCharacters">Similar Characters : </label><input
                    name="SimilarCharacters" type="checkbox" <?= $pwdgen->isBSimilarCharacters() ? 'checked' : '' ?>>
            </li>
            <li class="list-group-item text-center"><label for="Length">Length : </label><input name="Length"
                                                                                                type="number" min="0"
                                                                                                max="999"
                                                                                                value="<?= $pwdgen->getILength() ?>">
            </li>
        </ul>
    </form>
</div>
</body>
</html>